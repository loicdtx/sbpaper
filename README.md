## Reconstructing land use history from Landsat time-series: Case study of a swidden agriculture system in Brazil

Code and material used for the publication:

Loïc P. Dutrieux, Catarina C. Jakovac, Siti H. Latifah, Lammert Kooistra, [Reconstructing land use history from Landsat time-series: Case study of a swidden agriculture system in Brazil](http://dx.doi.org/10.1016/j.jag.2015.11.018), International Journal of Applied Earth Observation and Geoinformation, Volume 47, May 2016, Pages 112-124, ISSN 0303-2434, http://dx.doi.org/10.1016/j.jag.2015.11.018.

