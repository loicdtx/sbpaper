
predictNbBreaks <- function(df, model, spdf) {
  df <- df %>%
    mutate(magnitude = SegYBegin_post - SegYEnd_pre)
  
  df$predicted <- predict(model, df)
  
  # reshape output (one row per time-series)
  # Variables of interest (Just nb of burn breaks so far)
  # Number of burn breaks
  dfOut <- df %>%
    filter(predicted == 'Burn') %>%
    dplyr::select(id, Date) %>%
    group_by(id) %>%
    mutate(burnID=row_number()) %>%
    mutate(burn_ID = sprintf('burn_%d', burnID)) %>%
    dplyr::select(-burnID) %>%
    mutate(nburn = n()) %>%
    spread(burn_ID, Date)
  
  
  # input back into spdf
  # Create id field
  spdf@data$id <- rownames(spdf@data)
  spdf2 <- sp::merge(spdf, as.data.frame(dfOut), by = 'id')
  return(spdf2)
}
