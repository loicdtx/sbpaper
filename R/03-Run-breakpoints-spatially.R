# Require to install bfastSpatial from bitbucket (version that contains bpPhenoSp())
library(bfastSpatial)
library(magrittr)


# Load inputs (spatial polygons from 02)
segments <- readRDS('data/manioc_segments.rds') 

# Set path to NDMI brick
ndmiPath <- file.path(path, 'sbPaper/step/ndmi_stack.grd')


# Run bpPhenoSp()
ndmi <- brick(ndmiPath)
bpOut <- bpPhenoSp(x = ndmi, y = segments, formula = response ~ trend, h = 0.07, fun = mean, na.rm = TRUE, mc.cores = 8, maxFeatures = 8000)


# Write output to rds file
saveRDS(bpOut, 'data/breakpoints_spdf.rds', compress = 'xz')
