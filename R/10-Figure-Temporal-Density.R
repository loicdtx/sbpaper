library(dplyr)
library(RcppRoll)
library(ggplot2)
library(lubridate)

# TODO, add potential first and last break to the figure

# Set variables
h <- 0.06

dates <- readRDS('data/timeSeries_inSitu_samples_points.rds') %>%
  index() %>%
  data.frame(Date = .)

n <- nrow(dates)
nobs <- round(n * h)

duration <- dates %>%
  mutate(min = as.Date(roll_min(Date, nobs, align = 'center', fill = NA))) %>%
  mutate(max = as.Date(roll_max(Date, nobs, align = 'center', fill = NA))) %>%
  mutate(Duration = as.numeric(max - min)) %>%
  mutate(beginEnd = NA)

xIntercept <- duration$Date[c(nobs, n - nobs)]

gg <- ggplot(duration, aes(x = Date, y = Duration)) +
  geom_line() +
  geom_vline(xintercept = as.numeric(xIntercept), color = 'red', linetype = 'dashed') +
  xlab('Time') +
  ylab('Duration (days)') +
  theme_bw()

ggsave(gg, filename = 'latex/figs/minSegmentDuration.eps', width = 9, height = 4)
