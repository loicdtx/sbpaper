#!/bin/bash

# There must be a way to automatically generate the file name for the output from the latest git tag, something with git describe

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=latex/sbPaper_v0.1.1.pdf latex/sbPaper.pdf
