#!/bin/bash

# Update based on timestamp
if [ "$HOSTNAME" = vanoise ]; then
	rsync -avu dutri001@papaya:/media/DATA3/dutri001/sbPaper/ /media/dutri001/LP_DUTRIEUX_Data/RS/sbPaper/
	rsync -avu /media/dutri001/LP_DUTRIEUX_Data/RS/sbPaper/ dutri001@papaya:/media/DATA3/dutri001/sbPaper/
elif [ "$HOSTNAME" = papaya ]; then
	rsync -avu /media/DATA3/dutri001/sbPaper/ dutri001@vanoise:/media/dutri001/LP_DUTRIEUX_Data/RS/sbPaper/
	rsync -avu dutri001@vanoise:/media/dutri001/LP_DUTRIEUX_Data/RS/sbPaper/ /media/DATA3/dutri001/sbPaper/
fi

