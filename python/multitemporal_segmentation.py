#!/usr/bin/python
import os
import otbApplication

# From visual interpretation 1500 is clearly the right range radius
ranger = [1300, 1400, 1500]
spatialr = 3 # 5 if the OTB default ...
path = "/media/dutri001/LP_DUTRIEUX_Data/RS/sbPaper/step/"
inputRaster = "ndmi_stack_year_mean_clean.tif"
outputShape = "segmentation_results.sqlite"
for i in ranger:
	outputLayer = "mean_shift_range" + str(i) + "_radius" + str(spatialr)

	# Create an instance for segmentation
	Seg = otbApplication.Registry.CreateApplication("Segmentation")

	# Set segmentation parameters
	Seg.SetParameterString("in", os.path.join(path, inputRaster))

	Seg.SetParameterString("mode","vector")

	Seg.SetParameterString("mode.vector.out", os.path.join(path, outputShape)) # Though raster output would make it easier to clean afterward
	Seg.SetParameterString("mode.vector.layername", outputLayer)
	Seg.SetParameterString("mode.vector.fieldname", "id")
	Seg.SetParameterInt("mode.vector.startlabel", 1)
	Seg.SetParameterString("mode.vector.outmode", "ulu")
	Seg.SetParameterStringList("mode.vector.ogroptions", ["SPATIALITE=yes"])
	Seg.SetParameterInt("mode.vector.tilesize", 3000)

	Seg.SetParameterString("filter","meanshift")
	Seg.SetParameterInt("filter.meanshift.spatialr", spatialr) 
	Seg.SetParameterFloat("filter.meanshift.ranger", i)
	Seg.SetParameterInt("filter.meanshift.minsize", 3)

	# The following line execute the application
	Seg.ExecuteAndWriteOutput()