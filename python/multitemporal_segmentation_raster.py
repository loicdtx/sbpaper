#!/usr/bin/python
import os
import otbApplication

# From visual interpretation 1500 is clearly the right range radius
ranger = 1500
path = "/media/dutri001/LP_DUTRIEUX_Data/RS/sbPaper/step/"
inputRaster = "ndmi_stack_year_mean_clean.tif"
outputShape = "segments_mean_shift_" + str(ranger) + ".tif"

# Create an instance for segmentation
Seg = otbApplication.Registry.CreateApplication("Segmentation")

# Set segmentation parameters
Seg.SetParameterString("in", os.path.join(path, inputRaster))

Seg.SetParameterString("mode","raster")

Seg.SetParameterString("mode.raster.out", os.path.join(path, outputShape)) # Though raster output would make it easier to clean afterward

Seg.SetParameterString("filter","meanshift")
Seg.SetParameterInt("filter.meanshift.spatialr", 10) # 5 if the monteverdi default ...
Seg.SetParameterFloat("filter.meanshift.ranger", ranger)
Seg.SetParameterInt("filter.meanshift.minsize", 3)

# The following line execute the application
Seg.ExecuteAndWriteOutput()